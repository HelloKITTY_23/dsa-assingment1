import ballerina/graphql;




service /covid on new graphql:Listener(9000){

}
public type CovidStats record {|
    readonly string Code;
    string date;
    string region;
    decimal deaths?;
    decimal confirmed_cases?;
    decimal recovaries?;
    decimal tested?;
|};

table<CovidStats> key(Code) covidStatsTable = table [
    {Code: "KH", date: "12/09/2021", region: "Khomas", deaths: 39, confirmed_cases: 465, recovaries: 67, tested: 1200},
    {Code: "KS", date: "13", region: "Karas", deaths: 0, confirmed_cases: 78, recovaries: 3, tested: 34}
];


public distinct service class DiseaseData {
    private final readonly & CovidStats entry;

    function init(CovidStats entry) {
        self.entry= entry.cloneReadOnly();
    }

    resource function get Code () returns string{
        return self.entry.Code;
    }

    resource function get date () returns string{
        return self.entry.date;
    }

    resource function get region () returns string{
        return self.entry.region;
    }

    resource function get deaths () returns decimal?{
        if self.entry.deaths is decimal{
            return self.entry.deaths/1000;
        }
        return;

    }

    resource function get confirmed_cases () returns decimal?{
        if self.entry.confirmed_cases is decimal{
            return self.entry.confirmed_cases/1000;
        }
        return;
    }

    resource function  get tested () returns decimal?{
        if self.entry.tested is decimal{
            return self.entry.tested/1000;
        }
        return; 
    }

}


