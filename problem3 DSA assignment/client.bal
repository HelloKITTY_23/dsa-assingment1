import ballerina/io;
import ballerina/http;

http:Client studentRcrdC = check new http:Client("http://localhost:9090/question3");

public function main() returns error?{
     json response = check studentRcrdC -> post("/newStudents",[{studentNumber:1,name:"Len",email:"len@gmail.com",courses:{"Course Code":"DSA621s",
  Given_Assessments:"quiiz 2", "Weight":"50%","Marks":"35%"}}]);
  io:print(response.toJson());

  json response2 = check studentRcrdC -> put("/update/1",[{studentNumber:1,name:"Sam",email:"sam@yahoo.com", courses:{"Course Code":"DSA621s",
  "Given_Assessments":"quiz 3", "Weight":"50%","Marks":"70%"}}]);
  io:println(response2.toBalString());

    json response3 = check studentRcrdC -> put("/updateCourseDetails/1",{"Course Code":"DSA621s",
  "Given_Assessments":"quiz 4", "Weight":"50%","Marks":"60%"});
  io:println(response3);

    json response4 = check studentRcrdC -> get("/all");
    io:println(response4);

}