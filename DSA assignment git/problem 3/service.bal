import ballerina/http;
import ballerina/io;

type Students record {|
    readonly int studentId ;
    string emailA;
    string studentName;
    map<string> courses;
|};

 public type studentIdConflct record {|
    *http:Conflict;
    ErrorMessage body;
|};

 public type studentIdMissing record {|
    *http:NotFound;
    ErrorMessage body;
|};

public  type ErrorMessage record {|
    string errormsg;
|};

table<Students> key(studentId) studentsTable = table[];

 type createStudentEntries record {|
    *http:Created;
     Students[] body;
|};



service /question3 on new http:Listener(9090){

    resource function put updateStudent/[int studentId](@http:Payload Students[] updateEntry) returns createStudentEntries{
        lock{
        foreach Students item in studentsTable{
            if studentId == item.studentId {
                updateEntry.forEach(Students => studentsTable.put(Students));
            }else{
                io:println("no student found");
            }
        }
        return<createStudentEntries>{body:updateEntry};                           
        }
    }

    resource function post nStudents(@http:Payload Students[] studentEntries) returns createStudentEntries{
        lock{
        studentEntries.forEach(Students => studentsTable.add(Students));
        return <createStudentEntries>{body:studentEntries};
    }
   }

    resource function put updateCourse/[int studentNum](@http:Payload map<string> courses){
        foreach Students stdnt in studentsTable{
            if  studentNum == stdnt.studentId{
                stdnt.courses = courses;
            } 
        }
    }

      resource function delete remove/[int studentId](){
    foreach Students item in studentsTable {
        if item.studentId == studentId{
            Students removeStdnt = studentsTable.remove(item.studentId);
            io:print(removeStdnt);
        }
    }
    }


    resource function get allstdnt() returns Students|anydata{
        lock {
            return studentsTable.clone();
        }
    }


     resource function get singleStudent/[int studentId]()returns Students|anydata {
        lock {
            foreach Students item in studentsTable {
                if(studentId == item.studentId){
                    return studentsTable.get(item.studentId);
                }else{
                    return "no student found";
                }
            }           
        }        
    }
}
